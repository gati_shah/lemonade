// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util');
    sass   = require('gulp-sass');
    


// create a default task and just log a message
gulp.task('default', function() {
  return gutil.log('Gulp is running!')
});

gulp.task('default', ['watch', 'copyHtml','copyImg','copyJS' ,'copyFonts']);

gulp.task('copyHtml', function() {
  // copy any html files in source/ to public/
  gulp.src('source/*.html').pipe(gulp.dest('dist'));
});

gulp.task('copyImg', function() {
  // copy any images  in source/ to public/
  gulp.src('source/img/**/*.{png,jpg}').pipe(gulp.dest('dist/img'));
});

gulp.task('copyJS', function() {
  // copy any JS files  in source/ to public/
  gulp.src('source/javascript/**/*.js').pipe(gulp.dest('dist/js'));
});

gulp.task('copyFonts', function() {
  // copy any JS files  in source/ to public/
  gulp.src('source/fonts/georgia/**/*.ttf').pipe(gulp.dest('dist/stylesheets/fonts/georgia'));
});


gulp.task('build-css', function() {
  return gulp.src('source/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('dist/stylesheets'));
    
});




// configure which files to watch and what tasks to use on file changes
gulp.task('watch',['build-css'],function() {
  gulp.watch('source/scss/**/*.scss', ['build-css']);
  
});