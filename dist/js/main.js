

$(function() {
   var info = $('.featured');
    $.getJSON("list.json", function(data){
       /* sort json data */
        data.cats.sort(sort_by('id', false, parseInt));    
        $.each(data.cats, function(index, value){
            
            htmlData = '<div class="col-sm-6 text-center"><a href="'+value.link+'" target="_blank"><img src="'+ value.image +'" alt="" /><h4>'+ value.heading +'</h4><p>'+ value.content +'</p></a></div>';
            
            $('.featured').append(htmlData);
            
        }); 
    });  
});

var sort_by = function(field, reverse, primer){
   var key = primer ? 
       function(x) {return primer(x[field])} : 
       function(x) {return x[field]};
   reverse = !reverse ? 1 : -1;
   return function (a, b) {
       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
     } 
}


